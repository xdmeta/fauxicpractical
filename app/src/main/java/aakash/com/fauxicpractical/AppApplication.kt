package aakash.com.fauxicpractical

import aakash.com.fauxicpractical.injection.components.AppComponents
import aakash.com.fauxicpractical.injection.module.RoomDbModule
import aakash.com.fauxicpractical.injection.components.DaggerAppComponents
import android.app.Application

class AppApplication : Application() {
    lateinit var mComponent: AppComponents
    companion object {
        lateinit var instance : AppApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initDagger()
    }

    private fun initDagger() {
        mComponent = DaggerAppComponents.builder()
            .roomDbModule(RoomDbModule())
            .build()
    }
}