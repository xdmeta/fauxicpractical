package aakash.com.fauxicpractical.ui.saleDetails.viewmodel

import aakash.com.fauxicpractical.common.db.SalesDatabase
import aakash.com.fauxicpractical.common.db.SalesEntity
import aakash.com.fauxicpractical.ui.saleDetails.model.DataStates
import aakash.com.fauxicpractical.ui.saleDetails.model.Error
import aakash.com.fauxicpractical.ui.saleDetails.model.Result
import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import kotlin.collections.ArrayList

class SalesRepo @Inject constructor(val salesDb: SalesDatabase) {

    fun fetchAllData(dataModel: MutableLiveData<DataStates>, compositeDisposable: CompositeDisposable) {

        salesDb.SalesDao()
            .fetchAllItems()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(onSuccess = {
            if (it.isNotEmpty()) {
                dataModel.value = DataStates(result = Result(ArrayList(it)))
            } else {
                dataModel.value = DataStates(error = Error(msg = "No data available", show = true))
            }
        }, onError = {
            dataModel.value = DataStates(
                error = Error(
                    msg = it.message ?: "Something wrong happen",
                    show = true
                )
            )
        }).addTo(compositeDisposable)
    }

    fun deleteItem(salesEntity: SalesEntity, compositeDisposable: CompositeDisposable) {
        Observable.just(1)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onNext = {
                    salesDb.SalesDao().deleteItem(salesEntity)
                },
                onError = {

                }
            ).addTo(compositeDisposable)
    }

    fun updatePendingAmount(salesEntity: SalesEntity, compositeDisposable: CompositeDisposable) {
        Observable.just(1)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onNext = {
                    salesDb.SalesDao().updatePendingAmount(salesEntity)
                },
                onError = {

                }
            ).addTo(compositeDisposable)
    }

}