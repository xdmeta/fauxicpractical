package aakash.com.fauxicpractical.ui.itemDetails.view

import aakash.com.fauxicpractical.AppApplication
import aakash.com.fauxicpractical.R
import aakash.com.fauxicpractical.common.db.SalesEntity
import aakash.com.fauxicpractical.common.extensions.*
import aakash.com.fauxicpractical.common.helper.BaseActivity
import aakash.com.fauxicpractical.ui.itemDetails.model.SalesItem
import aakash.com.fauxicpractical.ui.itemDetails.viewmodel.AddItemViewModel
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.Window
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.button.MaterialButton
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_new_sale.*
import org.jetbrains.anko.toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import java.util.*
import javax.inject.Inject

class NewSaleActivity : BaseActivity(), DatePickerDialog.OnDateSetListener {

    private val itemList = ArrayList<SalesItem>()
    private var selectedDate: String = ""
    private var addItemDialog: AlertDialog? = null
    private var datePicker: DatePickerDialog? = null
    private var totalAmount: Double = 0.0
    private val addItemSubject = PublishSubject.create<Int>()

    private var totalQuantity = 0.0
    private var subTotal = 0.0
    private var amountPaid: Double = 0.0
    private val todayDate = Date()

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val addItemViewModel: AddItemViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[AddItemViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as AppApplication).mComponent.inject(this)
        setupUi()
    }

    private fun setupUi() {
        selectedDate = todayDate.formatToLocalDate()
        setContentView(R.layout.activity_new_sale)

        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.bgWhite))
        supportActionBar?.title = getString(R.string.title_activity_new_sale)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        observeViewModel()

        setupRecyclerView()

        setupDatePicker()

        setupItemDialog()

        setupClicks()

        setupAmountPaidObserver()
    }

    private fun setupAmountPaidObserver() {
        etAmountPaid.textChanges()
            .filter{ it.isNotEmpty() }
            .map { it.toString().toDouble() }
            .subscribeBy(onNext = {
                if (it == subTotal) {
                    cb_is_bill_paid.isChecked = true
                } else if (it > subTotal) {
                    cb_is_bill_paid.isChecked = false
                    rlParent.snack("Paid amount must be less then total amount")
                    etAmountPaid.setText(0.toString())
                } else {
                    cb_is_bill_paid.isChecked = false
                    amountPaid = it
                }
            }, onError = {
                it.printStackTrace()
            }).addTo(compositeDisposable)
    }

    private fun observeViewModel() {
        addItemViewModel.userModel.observe(this, Observer {
            when {
                it.error.show -> {
                    rlParent.snack(it.error.msg)
                }
                it.response.isSuccess -> {
                    rlParent.snack("Data added successfully!")
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }
        })
    }

    private fun setupClicks() {
        btnSubmit.clicks().subscribe {
            val pendingAmount = subTotal - amountPaid.toLong()
            addItemViewModel.addSalesEntity(
                SalesEntity(
                    customerName = etCustomerName.text.toString(),
                    salesDate = selectedDate,
                    pendingAmount = pendingAmount,
                    totalAmount = subTotal
                ), compositeDisposable
            )
        }.addTo(compositeDisposable)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (it.itemId) {
                android.R.id.home -> super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        selectedDate = "$year-$month-$dayOfMonth"
        tvPurchaseDate.text = selectedDate
    }

    private fun setupRecyclerView() {
        rvItemList.layoutManager = LinearLayoutManager(this)
        rvItemList.adapter = ItemListAdapter(itemList)
        addItemSubject.subscribe {
            totalQuantity += itemList[itemList.size - 1].quantity
            tvTotalQuantity.text = totalQuantity.toTwoDecimalPoints().toString()

            subTotal += itemList[itemList.size - 1].totalAmount
            tvSubTotal.text = subTotal.toTwoDecimalPoints().toString()

            if (itemList.size == 1) {
                rvItemList.visible()
                tvEmptyItem.gone()
            }
            rvItemList.adapter?.notifyItemInserted(itemList.size)
        }.addTo(compositeDisposable)
    }

    private fun setupDatePicker() {
        tvPurchaseDate.text = selectedDate
        tvPurchaseDate.clicks().subscribeBy(
            onNext = {
                datePicker = DatePickerDialog(
                    this,
                    this, todayDate.getLocalYear(),
                    todayDate.getLocalMonth(),
                    todayDate.getLocalDay()
                )
                datePicker?.show()
            },
            onError = {
                it.printStackTrace()
            }
        ).addTo(compositeDisposable)
    }

    private fun setupItemDialog() {
        fab.setOnClickListener {
            addItemDialog = AlertDialog.Builder(this).create()
            addItemDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            addItemDialog?.setView(
                LayoutInflater.from(this)
                    .inflate(R.layout.dialog_sales_details, rlParent, false)
            )
            (addItemDialog as AlertDialog).apply {
                show()
                val etItemName = findViewById<EditText>(R.id.etItemName)!!
                val etItemRate = findViewById<EditText>(R.id.etItemRate)!!
                val etQuantity = findViewById<EditText>(R.id.etQuantity)!!
                val tvTotalAmount = findViewById<TextView>(R.id.tvTotalAmount)!!

                var quantity = 0.0
                var itemRate = 0.0
                etItemRate.textChanges().filter { it.isNotEmpty() }.subscribe {
                    itemRate = it.toString().toDouble()
                    totalAmount = (itemRate * quantity).postTaxAmount()
                    tvTotalAmount.text = totalAmount.toString()
                }.addTo(compositeDisposable)

                etQuantity.textChanges().filter { it.isNotEmpty() }.subscribe {
                    quantity = it.toString().toDouble()
                    totalAmount = (itemRate * quantity).postTaxAmount()
                    tvTotalAmount.text = totalAmount.toString()
                }.addTo(compositeDisposable)

                findViewById<MaterialButton>(R.id.btnAdd)?.setOnClickListener {
                    if (etItemName.text.isEmpty() ||
                        etItemRate.text.isEmpty() ||
                        etQuantity.text.isEmpty()
                    ) {
                        this.context.toast(getString(R.string.err_empty_field))
                    } else {
                        this.dismiss()
                        itemList.add(
                            SalesItem(
                                etItemName.text.toString(),
                                quantity, itemRate, totalAmount
                            )
                        )
                        addItemSubject.onNext(0)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        dismissDialogs()
    }

    override fun onPause() {
        super.onPause()
        dismissDialogs()
    }

    private fun dismissDialogs() {
        addItemDialog.dismiss()
        datePicker.dismiss()
    }

    private fun Dialog?.dismiss() {
        this?.apply {
            if (isShowing) dismiss()
        }
    }
}
