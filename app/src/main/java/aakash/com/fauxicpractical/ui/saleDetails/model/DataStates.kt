package aakash.com.fauxicpractical.ui.saleDetails.model

import aakash.com.fauxicpractical.common.db.SalesEntity

data class Error(val msg: String = "", val show: Boolean = false)

data class Result(val itemList: ArrayList<SalesEntity> = ArrayList())

data class DataStates(val error: Error = Error(), val result: Result = Result())