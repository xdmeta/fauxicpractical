package aakash.com.fauxicpractical.ui.itemDetails.model

import aakash.com.fauxicpractical.common.db.SalesEntity

data class Error(val msg: String, val show: Boolean)

data class Response(val isSuccess: Boolean)

data class UserStates(
    val error: Error = Error("", false),
    val response: Response = Response(false)
)