package aakash.com.fauxicpractical.ui.itemDetails.viewmodel

import aakash.com.fauxicpractical.common.db.SalesDatabase
import aakash.com.fauxicpractical.common.db.SalesEntity
import aakash.com.fauxicpractical.ui.itemDetails.model.Error
import aakash.com.fauxicpractical.ui.itemDetails.model.Response
import aakash.com.fauxicpractical.ui.itemDetails.model.UserStates
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class AddItemViewModel @Inject constructor(val salesDatabase: SalesDatabase) :
    ViewModel() {
    val userModel = MutableLiveData<UserStates>()
    fun addSalesEntity(salesEntity: SalesEntity, compositeDisposable: CompositeDisposable) {
        when {
            salesEntity.customerName.isEmpty() ->
                userModel.value = UserStates(error = Error("Please enter customer name", true))
            salesEntity.pendingAmount < 0.0 ->
                userModel.value = UserStates(error = Error("Please valid pending name", true))
            salesEntity.totalAmount <= 0.0 ->
                userModel.value = UserStates(error = Error("Please add atleast one item", true))
            else -> {
                userModel.value = UserStates(response = Response(true))
                Observable.just(0)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribeBy(onNext = {
                        salesDatabase.SalesDao().insertSalesItem(salesEntity)
                    }, onError = {
                        it.printStackTrace()
                    }).addTo(compositeDisposable)
            }
        }
    }
}