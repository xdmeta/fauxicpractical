package aakash.com.fauxicpractical.ui.saleDetails.view

import aakash.com.fauxicpractical.AppApplication
import aakash.com.fauxicpractical.R
import aakash.com.fauxicpractical.common.db.SalesEntity
import android.app.SearchManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.transition.TransitionManager
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_sale_details_screen.*
import aakash.com.fauxicpractical.common.extensions.hideKeyboard
import aakash.com.fauxicpractical.common.extensions.isVisible
import aakash.com.fauxicpractical.common.extensions.snack
import aakash.com.fauxicpractical.common.helper.BaseActivity
import aakash.com.fauxicpractical.ui.itemDetails.view.NewSaleActivity
import aakash.com.fauxicpractical.ui.saleDetails.viewmodel.SalesViewModel
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.Window
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.button.MaterialButton
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.content_sale_details_screen.*
import org.jetbrains.anko.startActivityForResult
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SaleDetailsScreen : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val salesViewModel: SalesViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[SalesViewModel::class.java]
    }


    val searchSubject = PublishSubject.create<String>()
    private val salesItemList = ArrayList<SalesEntity>()
    private val allSalesItemsList = ArrayList<SalesEntity>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as AppApplication).mComponent.inject(this)
        setupUi()
        setupData()
        setupSearchObservable()
    }

    private fun setupData() {
        compositeDisposable = salesViewModel.compositeDisposable
        salesViewModel.fetchAllData()
        salesViewModel.salesModel.observe(this, Observer {
            tvError.visibility = it.error.show.isVisible()
            rvSalesData.visibility = it.result.itemList.isNotEmpty().isVisible()
            when {
                (it.error.show) -> {
                    tvError.text = it.error.msg
                }
                else -> {
                    allSalesItemsList.clear()
                    allSalesItemsList.addAll(it.result.itemList)

                    salesItemList.clear()
                    salesItemList.addAll(it.result.itemList)

                    rvSalesData.adapter?.notifyDataSetChanged()
                }
            }
        })
    }

    private fun setupSearchObservable() {
        searchSubject
            .debounce(200, TimeUnit.MILLISECONDS)
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { customerName ->
                    salesItemList.clear()
                    allSalesItemsList.forEach {
                        if (it.customerName.contains(customerName)) {
                            salesItemList.add(it)
                        }
                    }
                    rvSalesData.adapter?.notifyDataSetChanged()
                },
                onError = {
                    it.printStackTrace()
                }
            ).addTo(compositeDisposable)
    }

    private var updateAmountDialog: AlertDialog? = null
    private var updateAmountSubject = PublishSubject.create<Int>()

    private fun updatePendingAmount(position: Int, pendingAmount: Double) {
        updateAmountDialog = AlertDialog.Builder(this).create()
        updateAmountDialog?.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setView(
                LayoutInflater.from(this@SaleDetailsScreen)
                    .inflate(R.layout.dialog_update_pending_amount, rlParent, false)
            )
            show()
            val etAmountPaid = findViewById<EditText>(R.id.etAmountPaid)
            val btnPay = findViewById<MaterialButton>(R.id.btnPay)
            btnPay?.clicks()?.subscribe {
                if (etAmountPaid?.text?.toString()?.isEmpty() == false &&
                    pendingAmount > (etAmountPaid.text?.toString()?.toDouble() ?: 0.0)
                ) {
                    val index =
                        allSalesItemsList.indexOf(allSalesItemsList.filter { it.salesIndex == salesItemList[position].salesIndex }[0])
                    allSalesItemsList[index].pendingAmount =
                        pendingAmount - (etAmountPaid.text?.toString()?.toDouble() ?: 0.0)
                    salesViewModel.updatePendingAmount(allSalesItemsList[index])

                    salesItemList[position].pendingAmount =
                        pendingAmount - (etAmountPaid.text?.toString()?.toDouble() ?: 0.0)
                    updateAmountSubject.onNext(position)
                    dismiss()
                }
            }?.addTo(compositeDisposable)
        }

    }

    private fun setupUi() {
        setContentView(R.layout.activity_sale_details_screen)
        setSupportActionBar(toolbar)

        updateAmountSubject.subscribe { position ->
            rvSalesData?.adapter?.notifyItemChanged(position)
        }.addTo(compositeDisposable)

        rvSalesData.layoutManager = LinearLayoutManager(this)
        rvSalesData.adapter = SalesAdapter(salesItemList, { position ->
            val index =
                allSalesItemsList.indexOf(allSalesItemsList.filter { it.salesIndex == salesItemList[position].salesIndex }[0])
            salesViewModel.deleteEntity(allSalesItemsList[index])
            allSalesItemsList.removeAt(index)

            salesItemList.removeAt(position)
            rvSalesData.adapter?.notifyItemRemoved(position)

        }, { position, pendingAmount ->
            updatePendingAmount(position, pendingAmount)
        })
        (rvSalesData.adapter as SalesAdapter).notifyDataSetChanged()
        fab.setOnClickListener {
            startActivityForResult<NewSaleActivity>(ADD_NEW_ITEM_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    ADD_NEW_ITEM_REQUEST -> {
                        setupData()
                    }
                }
            }
        }
    }

    private val ADD_NEW_ITEM_REQUEST = 300


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_sale_details_screen, menu)

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.search).actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(p0: String?): Boolean {
                    hideKeyboard(this@apply)
                    return true
                }

                override fun onQueryTextChange(p0: String?): Boolean {
                    p0?.let {
                        searchSubject.onNext(it)
                    }
                    return true
                }

            })
        }

        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(findViewById(R.id.toolbar))
                }
                item.expandActionView()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
