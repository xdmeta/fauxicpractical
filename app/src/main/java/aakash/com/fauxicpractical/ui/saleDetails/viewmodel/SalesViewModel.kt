package aakash.com.fauxicpractical.ui.saleDetails.viewmodel

import aakash.com.fauxicpractical.common.db.SalesEntity
import aakash.com.fauxicpractical.ui.saleDetails.model.DataStates
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SalesViewModel @Inject constructor(private val salesRepo: SalesRepo) : ViewModel() {
    val salesModel = MutableLiveData<DataStates>()
    val compositeDisposable = CompositeDisposable()

    fun fetchAllData() {
        salesRepo.fetchAllData(salesModel, compositeDisposable)
    }

    fun deleteEntity(salesEntity: SalesEntity) {
        salesRepo.deleteItem(salesEntity, compositeDisposable)
    }

    fun updatePendingAmount(salesEntity: SalesEntity) {
        salesRepo.updatePendingAmount(salesEntity, compositeDisposable)
    }
}