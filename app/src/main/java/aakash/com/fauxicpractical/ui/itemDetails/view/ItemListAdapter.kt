package aakash.com.fauxicpractical.ui.itemDetails.view

import aakash.com.fauxicpractical.R
import aakash.com.fauxicpractical.common.extensions.toTwoDecimalPoints
import aakash.com.fauxicpractical.ui.itemDetails.model.SalesItem
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_sale_item.view.*

class ItemListAdapter(private val itemList: ArrayList<SalesItem>) :
    RecyclerView.Adapter<ItemListAdapter.ItemListHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemListHolder {
        return ItemListHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_sale_item, parent, false)
        )
    }

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: ItemListHolder, position: Int) {
        holder.itemView.apply {
            itemList[position].let {
                tvIndex.text = (position + 1).toString()
                tvItemName.text = it.itemName
                tvQty.text = it.quantity.toTwoDecimalPoints().toString()
                tvRate.text = it.price.toTwoDecimalPoints().toString()
                tvTotalAmount.text = it.totalAmount.toTwoDecimalPoints().toString()
            }
        }
    }

    class ItemListHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}