package aakash.com.fauxicpractical.ui.saleDetails.view

import aakash.com.fauxicpractical.R
import aakash.com.fauxicpractical.common.db.SalesEntity
import aakash.com.fauxicpractical.common.extensions.toTwoDecimalPoints
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.layout_paid_amount.view.*

class SalesAdapter(
    private val itemList: ArrayList<SalesEntity>,
    private val onItemDelete: (position: Int) -> Unit,
    private val onItemUpdate: (position: Int, pendingAmount: Double) -> Unit
) :
    RecyclerView.Adapter<SalesAdapter.SalesHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalesHolder {
        return SalesHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_paid_amount,
                null
            )
        )
    }

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: SalesHolder, position: Int) {
        holder.itemView.apply {
            itemList[position].let { it1 ->
                tvCustomerName.text = it1.customerName
                tvSalesDate.text = it1.salesDate
                tvTotalAmount.text = it1.totalAmount.toTwoDecimalPoints().toString()
                tvPendingAmount.text = it1.pendingAmount.toTwoDecimalPoints().toString()
                btnPay.setOnClickListener { onItemUpdate(position, it1.pendingAmount) }
                btnDelete.setOnClickListener { onItemDelete(position) }
            }
        }
    }


    class SalesHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}