package aakash.com.fauxicpractical.ui.itemDetails.model

data class SalesItem(
    val itemName: String,
    val quantity: Double,
    val price: Double,
    val totalAmount: Double
)