package aakash.com.fauxicpractical.injection.module

import aakash.com.fauxicpractical.ui.itemDetails.viewmodel.AddItemViewModel
import aakash.com.fauxicpractical.ui.saleDetails.viewmodel.SalesViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass


@Singleton
class ViewModelFactory @Inject constructor(private val viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T = viewModels[modelClass]?.get() as T
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(AddItemViewModel::class)
    internal abstract fun addItemViewModel(viewModel: AddItemViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SalesViewModel::class)
    internal abstract fun salesViewModel(viewModel: SalesViewModel): ViewModel
    //Add more ViewModels here
}