package aakash.com.fauxicpractical.injection.module

import aakash.com.fauxicpractical.AppApplication
import aakash.com.fauxicpractical.common.db.SalesDatabase
import aakash.com.fauxicpractical.common.helper.AppConstants
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomDbModule {
    @Provides
    @Singleton
    fun provideRoomDatabase(): SalesDatabase =
        Room.databaseBuilder(
            AppApplication.instance,
            SalesDatabase::class.java,
            AppConstants.DB_SALES
        )
            .fallbackToDestructiveMigration()
            .build()
}