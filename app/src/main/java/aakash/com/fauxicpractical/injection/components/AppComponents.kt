package aakash.com.fauxicpractical.injection.components

import aakash.com.fauxicpractical.injection.module.RoomDbModule
import aakash.com.fauxicpractical.injection.module.ViewModelModule
import aakash.com.fauxicpractical.ui.itemDetails.view.NewSaleActivity
import aakash.com.fauxicpractical.ui.saleDetails.view.SaleDetailsScreen
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(
    modules = [RoomDbModule::class,
        ViewModelModule::class]
)
interface AppComponents {
    fun inject(activity: SaleDetailsScreen)
    fun inject(activity: NewSaleActivity)
}