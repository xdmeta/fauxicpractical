package aakash.com.fauxicpractical.common.db

import aakash.com.fauxicpractical.common.helper.AppConstants
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = AppConstants.TABLE_SALES)
data class SalesEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = AppConstants.COLUMN_SALE_INDEX, index = true)
    var salesIndex : Int = 0,

    @ColumnInfo(name = AppConstants.COLUMN_CUSTOMER_NAME)
    var customerName : String = "",

    @ColumnInfo(name = AppConstants.COLUMN_PURCHASE_DATE)
    var salesDate : String = "",

    @ColumnInfo(name = AppConstants.COLUMN_TOTAL_AMOUNT)
    var totalAmount : Double = 0.0,

    @ColumnInfo(name = AppConstants.COLUMN_PENDING_AMOUNT)
    var pendingAmount : Double = 0.0
)