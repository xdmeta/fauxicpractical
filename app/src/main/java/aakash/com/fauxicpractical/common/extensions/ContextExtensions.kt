package aakash.com.fauxicpractical.common.extensions

import aakash.com.fauxicpractical.AppApplication
import android.app.Activity
import android.content.*
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.StringRes
import org.jetbrains.anko.internals.AnkoInternals

/**
 * Created by stllpt065 on 6/7/17.
 */
/**
 * Toast to display String as Charsequence
 */
fun Context.charToast(char: CharSequence) = Toast.makeText(this, char, Toast.LENGTH_LONG).show()

/**
 * Toast to display String from Resource file as int
 */
fun Context.resToast(res: Int) = Toast.makeText(this, res, Toast.LENGTH_SHORT).show()

fun Context.longCharToast(char: CharSequence) = Toast.makeText(this, char, Toast.LENGTH_LONG).show()

inline fun <reified T : Activity> Activity.startActivityIfRunning(vararg params: Pair<String, Any>) {

    if (!isDestroyed) {
        AnkoInternals.internalStartActivity(this, T::class.java, params)
    }
}

inline fun <reified T : Activity> Activity.startActivityWithNoAnimation(vararg params: Pair<String, Any>) {
    AnkoInternals.internalStartActivity(this, T::class.java, params)
}


fun Activity.resToast(@StringRes res: Int) {
    val li = this.layoutInflater
    val toast = Toast(this)
    toast.apply {
        duration = Toast.LENGTH_SHORT
        setText(res)
        show()
    }
}

fun Activity.resToast(message: String) {
    val li = this.layoutInflater
    val toast = Toast(this)
    toast.apply {
        duration = Toast.LENGTH_SHORT
        setText(message)
        show()
    }
}

fun Context.hideKeyboard(view: View?) {
    val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view?.windowToken, 0)
    imm.showSoftInput(view, 0)
}

fun Context.showKeyBoard(view: View?) {
    val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun log(msg: Any?, tag: String? = null) {
    val tagName = tag?.let {
        it
    } ?: AppApplication.instance.applicationContext.applicationInfo.name+"Logs"

    Log.d(tagName, "$msg")

}

fun Throwable.printDebugStackTrace() {
    this.printStackTrace()

}