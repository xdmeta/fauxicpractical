package aakash.com.fauxicpractical.common.db

import aakash.com.fauxicpractical.common.helper.AppConstants
import androidx.room.*
import io.reactivex.Single

@Dao
public interface SalesDao {

    @Query("Select * from ${AppConstants.TABLE_SALES}")
    fun fetchAllItems(): Single<List<SalesEntity>>

    @Insert
    fun insertSalesItem(salesItem: SalesEntity)

    @Delete
    fun deleteItem(salesEntity: SalesEntity)

    @Update
    fun updatePendingAmount(salesEntity: SalesEntity)
}