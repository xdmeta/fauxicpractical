package aakash.com.fauxicpractical.common.extensions

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by aakash on 7/2/18.
 */
// Server date formats
val SERVER_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
val SERVER_DATE_FORMAT_WT_MS = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())

// Local date formats
val LOCAL_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
val LOCAL_DAY_FORMAT = SimpleDateFormat("dd", Locale.getDefault())
val LOCAL_MONTH_FORMAT = SimpleDateFormat("MM", Locale.getDefault())
val LOCAL_YEAR_FORMAT = SimpleDateFormat("yyyy", Locale.getDefault())

fun Date.formatToLocalDate() = LOCAL_DATE_FORMAT.format(this)

fun Date.getLocalYear() = LOCAL_YEAR_FORMAT.format(this).toInt()

fun Date.getLocalMonth() = LOCAL_MONTH_FORMAT.format(this).toInt()

fun Date.getLocalDay() = LOCAL_DAY_FORMAT.format(this).toInt()


@Throws(Exception::class)
fun Date.changeToServerDate() = SERVER_DATE_FORMAT.format(this.updateMonth(reduce = false))

fun Date.updateMonth(reduce: Boolean): Date {
    val cal = Calendar.getInstance()
    cal.time = this
    cal.add(Calendar.MONTH, if (reduce) -1 else 1)
    return cal.time
}

fun Long.wasBeforeSixHours() =
        ((Date().time - this) >= 6 * 3600 * 1000L)

fun String.getLocalDate(): Date {
    val cal = Calendar.getInstance()
    val date = SERVER_DATE_FORMAT_WT_MS.parse(this)
    cal.time = date
    cal.set(2000, 1, 1)
    return cal.time
}