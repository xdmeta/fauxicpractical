package aakash.com.fauxicpractical.common.helper

import android.content.Context
import android.content.res.Resources
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class FixedHeightRecyclerView : RecyclerView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        var heightSpec = heightSpec
        heightSpec = View.MeasureSpec.makeMeasureSpec(convertDpToPixel(120.toFloat()).toInt(), View.MeasureSpec.AT_MOST)
        super.onMeasure(widthSpec, heightSpec)
    }

    private fun convertDpToPixel(dp: Float): Float {
        val metrics = Resources.getSystem().getDisplayMetrics()
        val px = dp * (metrics.densityDpi / 160f)
        return Math.round(px).toFloat()
    }
}