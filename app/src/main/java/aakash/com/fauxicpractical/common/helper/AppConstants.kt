package aakash.com.fauxicpractical.common.helper

object AppConstants {

    // Database name
    const val DB_SALES = "dbSales"

    // Table name
    const val TABLE_SALES = "SalesMaster"

    // Column names
    const val COLUMN_SALE_INDEX = "Index"
    const val COLUMN_CUSTOMER_NAME = "CustomerName"
    const val COLUMN_PURCHASE_DATE = "PurchaseData"
    const val COLUMN_TOTAL_AMOUNT = "TotalAmount"
    const val COLUMN_PENDING_AMOUNT = "PendingAmount"
}