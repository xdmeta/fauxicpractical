package aakash.com.fauxicpractical.common.extensions

private const val CGST = 9
private const val SGST = 9

fun Double.postTaxAmount() = (this + (this * CGST / 100) + (this * SGST / 100)).toTwoDecimalPoints()

fun Double?.toTwoDecimalPoints() = String.format(
    "%.2f",
    this ?: 0.00
).toDouble()