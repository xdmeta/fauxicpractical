package aakash.com.fauxicpractical.common.db

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [SalesEntity::class], version = 1, exportSchema = false)
public abstract class SalesDatabase : RoomDatabase() {
    public abstract fun SalesDao() : SalesDao
}